

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $('.carousel').carousel({
    interval: 2000
  });
  $('#contacto').on('show.bs.modal',function(e){
    console.log('Estado: Comienza a abrirse');
    $('.btn-leer').removeClass('btn-primary');
    $('.btn-leer').addClass('btn-danger');
    $('.btn-leer').prop('disabled',true);
  });
  $('#contacto').on('shown.bs.modal',function(e){
    console.log('Estado: Se terminó de abrir');
  });
  $('#contacto').on('hide.bs.modal',function(e){
    console.log('Estado: Comienza a ocultarse');
  });
  $('#contacto').on('hidden.bs.modal',function(e){
    console.log('Estado: Se terminó de ocultar');
    $('.btn-leer').removeClass('btn-danger');
    $('.btn-leer').addClass('btn-primary');
    $('.btn-leer').prop('disabled',false);    
  });  
});
